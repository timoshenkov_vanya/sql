package com.example.demo.Violation;

import jakarta.persistence.*;


@Entity
@Table
public class Violation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String carNumber;
    private String violatorName;
    private String gayOfficerName;
    private String timeOfProtocol;

    private Integer Summ;
    private Boolean isSubpoena;
    private Boolean isPayment;
    private String dataOfPayment;
    private String deadlinePayment;



    public void setId(Integer id) {
        this.id = id;
    }
    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getViolatorName() {
        return violatorName;
    }

    public void setViolatorName(String violatorName) {
        this.violatorName = violatorName;
    }

    public String getGayOfficerName() {
        return gayOfficerName;
    }

    public void setGayOfficerName(String gayOfficerName) {
        this.gayOfficerName = gayOfficerName;
    }

    public String getTimeOfProtocol() {
        return timeOfProtocol;
    }

    public void setTimeOfProtocol(String timeOfProtocol) {
        this.timeOfProtocol = timeOfProtocol;
    }

    public Integer getSumm() {
        return Summ;
    }

    public void setSumm(Integer summ) {
        this.Summ = summ;
    }

    public Boolean getSubpoena() {
        return isSubpoena;
    }

    public void setSubpoena(Boolean subpoena) {
        this.isSubpoena = subpoena;
    }

    public Boolean getPayment() {
        return isPayment;
    }

    public void setPayment(Boolean payment) {
        this.isPayment = payment;
    }

    public String getDataOfPayment() {
        return dataOfPayment;
    }

    public void setDataOfPayment(String dataOfPayment) {
        this.dataOfPayment = dataOfPayment;
    }

    public String getDeadlinePayment() {
        return deadlinePayment;
    }

    public void setDeadlinePayment(String deadlinePayment) {
        this.deadlinePayment = deadlinePayment;
    }




    public Integer getId() {
        return this.id;
    }

    public void updateObject(Violation object) {
        this.setCarNumber(object.getCarNumber());
        this.setPayment(object.getPayment());
        this.setViolatorName(object.getViolatorName());
        this.setPayment(object.getPayment());
        this.setDeadlinePayment(object.getDeadlinePayment());
        this.setDataOfPayment(object.getDataOfPayment());
        this.setGayOfficerName(object.getGayOfficerName());
        this.setTimeOfProtocol(object.getTimeOfProtocol());
        this.setSubpoena(object.getSubpoena());
        this.setSumm(object.getSumm());

    }
}
